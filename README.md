# Database

## Create/Start
Start Postgres Database  and PostgresMyAdmin

``` bash
docker-compose up
```

## Reset data
Recreate/Reset DB with script folder

``` bash
docker-compose down
docker-compose up --force-recreate --renew-anon-volumes
```
## Use PGAdmin

Go to pgAdmin at http://localhost:5050
1) Login admin/admin
2) Add new server
3) name: DB
4) Connexion -> host: postgres
5) Connexion -> user: admin
6) Connexion -> password: admin
