CREATE DATABASE emergency_manager;
\c emergency_manager 

-- -----------------------------------------------------
-- Table capteur
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS "capteur" (
  idCapteur SERIAL,
  lat FLOAT NULL,
  long FLOAT NULL,
  valeur FLOAT NULL DEFAULT 0,
  PRIMARY KEY (idCapteur)
 );


-- -----------------------------------------------------
-- Table caserne
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS caserne (
  idCaserne SERIAL,
  lat FLOAT NULL,
  long FLOAT NULL,
  PRIMARY KEY (idCaserne)
);


-- -----------------------------------------------------
-- Table incident
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS incident (
  idIncident SERIAL,
  dateDebut DATE NOT NULL,
  dateFin DATE NULL DEFAULT NULL,
  lat FLOAT NULL,
  long FLOAT NULL,
  PRIMARY KEY (idIncident)
);


-- -----------------------------------------------------
-- Table camion
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS camion (
  idCamion SERIAL,
  lat FLOAT NULL,
  long FLOAT NULL,
  caserneAssociee INT NULL,
  affectationIncendie INT NULL DEFAULT NULL,
  PRIMARY KEY (idCamion),
  CONSTRAINT idCaserne
    FOREIGN KEY (caserneAssociee)
    REFERENCES caserne (idCaserne)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT attribution
    FOREIGN KEY (affectationIncendie)
    REFERENCES incident (idIncident)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

CREATE INDEX idCaserne_idx ON camion (caserneAssociee ASC);
CREATE INDEX attribution_idx ON camion (affectationIncendie ASC);

INSERT INTO capteur (lat, long) VALUES (45.776, 4.8601),
(45.773, 4.8601),
(45.77, 4.8601),
(45.767, 4.8601),
(45.764, 4.8601),
(45.761, 4.8601),
(45.758, 4.8601),
(45.755, 4.8601),
(45.752, 4.8601),
(45.749, 4.8601),
(45.776, 4.8631),
(45.773, 4.8631),
(45.77, 4.8631),
(45.767, 4.8631),
(45.764, 4.8631),
(45.761, 4.8631),
(45.758, 4.8631),
(45.755, 4.8631),
(45.752, 4.8631),
(45.749, 4.8631),
(45.776, 4.8661),
(45.773, 4.8661),
(45.77, 4.8661),
(45.767, 4.8661),
(45.764, 4.8661),
(45.761, 4.8661),
(45.758, 4.8661),
(45.755, 4.8661),
(45.752, 4.8661),
(45.749, 4.8661),
(45.776, 4.8691),
(45.773, 4.8691),
(45.77, 4.8691),
(45.767, 4.8691),
(45.764, 4.8691),
(45.761, 4.8691),
(45.758, 4.8691),
(45.755, 4.8691),
(45.752, 4.8691),
(45.749, 4.8691),
(45.776, 4.8721),
(45.773, 4.8721),
(45.77, 4.8721),
(45.767, 4.8721),
(45.764, 4.8721),
(45.761, 4.8721),
(45.758, 4.8721),
(45.755, 4.8721),
(45.752, 4.8721),
(45.749, 4.8721),
(45.776, 4.8751),
(45.773, 4.8751),
(45.77, 4.8751),
(45.767, 4.8751),
(45.764, 4.8751),
(45.761, 4.8751),
(45.758, 4.8751),
(45.755, 4.8751),
(45.752, 4.8751),
(45.749, 4.8751)